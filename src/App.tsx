import React, { ChangeEvent } from 'react';
import Select from 'react-select';
import './App.css';

type LabelValueObject = Object & {
  value: string,
  label: string
}

const selectOptions = [
  { value: 'Finansovaya gramotnost', label: 'Финансовая грамотность' },
  { value: 'Tsep postavok. Logistika', label: 'Цепь поставок. Логистика' },
  { value: 'Russkiy avangard 20-go veka', label: 'Русский авангард 20-го века' },
  { value: 'Teoriya Igr', label: 'Теория Игр' },
]

interface IColorPickerInputState {
  userName: string;
  userEmail: string;
  seminar: string;
  nameError: boolean;
  emailError: boolean;
  seminarError: boolean;
  loading: boolean;
}

class App extends React.Component<{}, IColorPickerInputState> {
  private myRef;
  constructor(props: {}) {
    super(props);
    this.myRef = React.createRef()
    this.state = {
      userName: '',
      userEmail: '',
      seminar: '',
      nameError: false,
      emailError: false,
      seminarError: false,
      loading: false,
    }
  }

  validateEmail = (email: string) => {
    const re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return re.test(email.toLowerCase());
  }

  handleChangeName = (value: string) => {
    this.setState({
      userName: value,
      nameError: false,
    })
  }

  handleChangeEmail = (value: string) => {
    this.setState({
      userEmail: value,
      emailError: false,
    })
  }

  handleChangeSeminar = (selectedOption: LabelValueObject | null) => {
    this.setState({
      seminar: selectedOption!.value ,
      seminarError: false,
    })
  }

  fade = (el: any) => {
    el.classList.remove('d-none')
    setTimeout(() => {
      el.style.opacity = 1
    }, 100)
    setTimeout(() => {
      el.style.opacity = 0;
    }, 4000)
    setTimeout(() => {
      el.classList.add('d-none')
    }, 5000)
  }

  isValid = () => {
    let valid = true
    if(!this.state.userName) {
      this.setState({ nameError: true })
      valid = false
    }
    if(!this.state.userEmail || !this.validateEmail(this.state.userEmail)) {
      this.setState({ emailError: true })
      valid = false
    }
    if(!this.state.seminar) {
      this.setState({ seminarError: true })
      valid = false
    }
    return valid
  }

  sendMessage = () => {
    const { loading, userName, userEmail, seminar } = this.state;
    if(this.state.loading) {
      return
    }
    if(!this.isValid()) {
      return
    }
    this.setState({ loading: true })
    const message = {
      userName: this.state.userName,
      userEmail: this.state.userEmail,
      seminar: this.state.seminar,
    }
    setTimeout(() => {
      // if(response) {
        this.setState({ loading: false }, () => {
          this.fade(this.myRef.current)
        })
      // }
    }, 1500);
  }

  render() {
    const { nameError, userName, emailError, userEmail, seminarError, loading } = this.state;
    return (
      <div className="background position-relative d-flex justify-content-center align-items-center h-100 p-2">
        <div className="background-block-1 background-block position-absolute rounded"></div>
        <div className="background-block-2 background-block position-absolute rounded"></div>
        <div className="background-block-3 background-block position-absolute rounded"></div>
        <div className="form-block rounded">
          <h1 className="mb-4">Отправить заявку на участие в семинаре</h1>
          <p className="mb-1">Организаторы свяжутся с вами для подтвержения записи.</p>
          <p className="mb-4">Участие в семинаре <u>бесплатное</u>.</p>
          <p className="mb-1">Ваше имя:</p>
          <div className="input-group mb-3">
            <input type="text" className={`form-control ${nameError ? 'is-invalid' : ''}`} placeholder="Иванов Алексей"
                   aria-label="Username" aria-describedby="basic-addon1"
                   value={userName}
                   onChange={ (e) => this.handleChangeName(e.target.value) } />
          </div>
          <p className="mb-1">Контактный email:</p>
          <div className="input-group mb-3">
            <input type="text" className={`form-control ${emailError ? 'is-invalid' : ''}`} placeholder="example@mail.com"
                   aria-label="Recipient's username" aria-describedby="basic-addon2"
                   value={userEmail}
                   onChange={ (e) => this.handleChangeEmail(e.target.value) } />
          </div>
          <p className="mb-1">Интересующий семинар:</p>
          <Select className={`mb-3 ${seminarError ? 'is-invalid' : ''}`}
                  options={selectOptions} id="seminarSelect" placeholder="Выбрать"
                  onChange={this.handleChangeSeminar} />
          <div className="send-block d-flex align-items-center pt-3">
            <div>
              <p className="mb-1">Все поля обязательны для заполнения.</p>
              <p>Отправляя заявку, вы соглашаетесь с договором публичной оферты и политикой обработки данных.</p>
            </div>
            <button type="button" className="btn btn-primary h-50 p-3 ml-5" onClick={this.sendMessage}>
              {loading ? (
                <div className="spinner-border" role="status">
                  <span className="visually-hidden">Loading...</span>
                </div>
              ) : (
                <label>Отправить</label>
              )}
            </button>
          </div>
          <div className="alert alert-info alert-dismissible fade position-absolute d-none" role="alert"
               style={{top: 0, right: 0}} ref={this.myRef as React.RefObject<HTMLDivElement>}>
            <div className="alert-text rounded">Ваша заявка успешно отправлена и находится в обработке. Ожидайте email с подтверждением бронирования</div>
          </div>
        </div>
      </div>
    )
  }
}

export default App;
